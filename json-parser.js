
let fs = require("fs");
let csv = require("fast-csv");

let csv_data_stream = fs.createReadStream("./samples.csv");
let csv_untreated_data = [];
let json_result = {
  bedroom: [],
  sunroom: [],
  study: [],
  studio: [],
  attic: [],
  solar: []
};


csvDataStream = csv().on("data", function(data){
     //console.log("data", data);
     csv_untreated_data.push(data);
})
.on("end", function(){

  console.log("csv_untreated_data.length", csv_untreated_data.length);
  for(let i in csv_untreated_data){
    //json_result.bedroom.push(csv_untreated_data[i][0]);
    json_result.bedroom.push(Number(csv_untreated_data[i][0]));
    json_result.sunroom.push(Number(csv_untreated_data[i][1]));
    json_result.study.push(Number(csv_untreated_data[i][2]));
    json_result.studio.push(Number(csv_untreated_data[i][3]));
    json_result.attic.push(Number(csv_untreated_data[i][4]));
    json_result.solar.push(Number(csv_untreated_data[i][5]));
  }

  //console.log("over and out", csv_untreated_data.length);
  console.log(json_result);
  console.log('SAVE JSON TO FILE!');

  fs.writeFileSync("./myapp/public/static_data.js", "var static_data = " + JSON.stringify(json_result,null,2), function(err) {
         if(err) {
             return console.log(err);
         }
         console.log("The file was saved!");
     });
});

console.log("Parsing..")
csv_data_stream.pipe(csvDataStream);
