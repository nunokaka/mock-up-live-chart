console.log("Hello")
console.log("static_data is:",typeof static_data);


Chart.pluginService.register({
 beforeDraw: function (chart) {
   if (chart.config.options.elements.center) {
     //Get ctx from string
     var ctx = chart.chart.ctx;

     //Get options from the center object in options
     var centerConfig = chart.config.options.elements.center;
     var fontStyle = centerConfig.fontStyle || 'Arial';
     var txt = centerConfig.text;
     var color = centerConfig.color || '#000';
     var sidePadding = centerConfig.sidePadding || 20;
     var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
     //Start with a base font of 30px
     ctx.font = "30px " + fontStyle;

     //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
     var stringWidth = ctx.measureText(txt).width;
     var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

     // Find out how much the font can grow in width.
     var widthRatio = elementWidth / stringWidth;
     var newFontSize = Math.floor(25 * widthRatio);
     var elementHeight = (chart.innerRadius * 2);

     // Pick a new font size so it will not be larger than the height of label.
     var fontSizeToUse = Math.min(newFontSize, elementHeight);

     //Set font settings to draw it correctly.
     ctx.textAlign = 'center';
     ctx.textBaseline = 'middle';
     var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
     var centerY = chart.chartArea.bottom - 10;
     ctx.font = fontSizeToUse+"px " + fontStyle;
     ctx.fillStyle = color;

     //Draw text in center
     ctx.fillText(txt, centerX, centerY);
   }
 }
});

$(document).ready(function(){


  const HALF_DONUT_OPTIONS = {
      rotation: 1 * Math.PI,
      circumference: 1 * Math.PI,
      elements: {
        center: {
          text: '',
          color: '#36A2EB', //Default black
          fontStyle: 'Helvetica', //Default Arial
          sidePadding: 15 //Default 20 (as a percentage)
        }
      },
      title: {
          display: false,
          text: 'X'
      }
    };

  let DEFAULT_HALF_DONUT_INIT = {
      type: 'doughnut',
      data: {
          labels: false, // [], //["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
          datasets: [{
              legend: {
                display: false,
              },
              label: 'Energy consumption',
              data: [0,0],
              backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(223, 223, 223,0.2)'
              ],
              borderColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(223, 223, 223, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: HALF_DONUT_OPTIONS
  };

  let TIME_INDEX = 0;

  /* Assining HTML elements to javascipt variables */
  const bedroomCtx = document.getElementById("half-donut-1");
  const sunroomCtx = document.getElementById("half-donut-2");
  const studyCtx = document.getElementById("half-donut-3");
  const studioCtx = document.getElementById("half-donut-4");
  const atticCtx = document.getElementById("half-donut-5");
  const totalCtx = document.getElementById("half-donut-6");
  const solarCtx = document.getElementById("half-donut-7");

  const linechartCtx = document.getElementById("line-chart-1");


  /* Initializing individual chart options from default opts */
  let bedroomInit = jQuery.extend(true, {},DEFAULT_HALF_DONUT_INIT);
  let sunroomInit = jQuery.extend(true, {},DEFAULT_HALF_DONUT_INIT);
  let studyInit = jQuery.extend(true, {},DEFAULT_HALF_DONUT_INIT);
  let studioInit = jQuery.extend(true, {},DEFAULT_HALF_DONUT_INIT);
  let atticInit = jQuery.extend(true, {},DEFAULT_HALF_DONUT_INIT);
  let totalInit = jQuery.extend(true, {},DEFAULT_HALF_DONUT_INIT);
  let solarInit = jQuery.extend(true, {},DEFAULT_HALF_DONUT_INIT);

  bedroomInit.data.datasets[0].backgroundColor[0] = 'rgba(255, 99, 132, 0.2)';
  bedroomInit.data.datasets[0].borderColor[0] = 'rgba(255,99,132,1)';
  //bedroomInit.options.title.text = 'Bedroom';

  sunroomInit.data.datasets[0].backgroundColor[0] = 'rgba(54, 162, 235, 0.2)';
  sunroomInit.data.datasets[0].borderColor[0] = 'rgba(54, 162, 235, 1)';
  //sunroomInit.options.title.text = 'Sunroom';

  studyInit.data.datasets[0].backgroundColor[0] = 'rgba(255, 206, 86, 0.2)';
  studyInit.data.datasets[0].borderColor[0] = 'rgba(255, 206, 86, 1)';
  //studyInit.options.title.text = 'Study';

  studioInit.data.datasets[0].backgroundColor[0] = 'rgba(75, 192, 192, 0.2)';
  studioInit.data.datasets[0].borderColor[0] = 'rgba(75, 192, 192, 1)';
  //studioInit.options.title.text = 'Studio';

  atticInit.data.datasets[0].backgroundColor[0] = 'rgba(153, 102, 255, 0.2)';
  atticInit.data.datasets[0].borderColor[0] = 'rgba(153, 102, 255, 1)';
  //atticInit.options.title.text = 'Attic';

  totalInit.data.datasets[0].backgroundColor[0] = 'rgba(255, 159, 64, 0.2)'
  totalInit.data.datasets[0].borderColor[0] = 'rgba(255, 159, 64, 1)'
  //totalInit.options.title.text = 'Total';

  solarInit.data.datasets[0].backgroundColor[0] = 'rgba(255, 99, 132, 0.2)';
  solarInit.data.datasets[0].borderColor[0] = 'rgba(255, 99, 132, 1)';
  //solarInit.options.title.text = 'Solar';

  window.bedroomHalfDonut = new Chart(bedroomCtx, bedroomInit);
  window.sunroomHalfDonut = new Chart(sunroomCtx, sunroomInit);
  window.studyHalfDonut = new Chart(studyCtx, studyInit);
  window.studioHalfDonut = new Chart(studioCtx, studioInit);
  window.atticHalfDonut = new Chart(atticCtx, atticInit);
  window.totalHalfDonut = new Chart(totalCtx, totalInit);
  window.solarHalfDonut = new Chart(solarCtx, solarInit);

  window.lineChart = new Chart(linechartCtx, {
  type: 'line',
  data: {
    labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
    datasets: [{
        data: [86,114,106,106,107,111,133,221,783,2478],
        label: "Africa",
        borderColor: "#3e95cd",
        fill: false
      }, {
        data: [282,350,411,502,635,809,947,1402,3700,5267],
        label: "Asia",
        borderColor: "#8e5ea2",
        fill: false
      }, {
        data: [168,170,178,190,203,276,408,547,675,734],
        label: "Europe",
        borderColor: "#3cba9f",
        fill: false
      }, {
        data: [40,20,10,16,24,38,74,167,508,784],
        label: "Latin America",
        borderColor: "#e8c3b9",
        fill: false
      }, {
        data: [6,3,2,2,7,26,82,172,312,433],
        label: "North America",
        borderColor: "#c45850",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'World population per region (in millions)'
    }
  }
});


  function nextMinute(){

    /* Getting the first value */
    var current_bedroom = Math.round( 10000 * static_data.bedroom[TIME_INDEX] )/100;
    var current_sunroom = Math.round( 10000 * static_data.sunroom[TIME_INDEX] )/100;
    var current_study = Math.round( 10000 * static_data.study[TIME_INDEX] )/100;
    var current_studio = Math.round( 10000 * static_data.studio[TIME_INDEX] )/100;
    var current_attic = Math.round( 10000 * static_data.attic[TIME_INDEX] )/100;
    var current_solar = Math.round( 10000 * static_data.solar[TIME_INDEX] )/100;

    var current_total = current_bedroom + current_sunroom
        + current_study + current_studio + current_attic;

    console.log(TIME_INDEX, current_bedroom,current_sunroom,current_study,current_studio,current_attic,current_solar);


    bedroomInit.options.elements.center.text = current_bedroom;
    sunroomInit.options.elements.center.text = current_sunroom;
    studyInit.options.elements.center.text = current_study;
    studioInit.options.elements.center.text = current_studio;
    atticInit.options.elements.center.text = current_attic;
    totalInit.options.elements.center.text = current_total;
    solarInit.options.elements.center.text = current_solar;

    bedroomInit.data.datasets[0].data = [ current_bedroom, current_total - current_bedroom ];
    sunroomInit.data.datasets[0].data = [ current_sunroom, current_total - current_sunroom ];
    studyInit.data.datasets[0].data = [ current_study, current_total - current_study ];
    studioInit.data.datasets[0].data = [ current_studio, current_total - current_studio ];
    atticInit.data.datasets[0].data = [ current_attic, current_total - current_attic ];
    totalInit.data.datasets[0].data = [ current_total, current_total - current_total ];
    solarInit.data.datasets[0].data = [ current_solar, current_total - current_solar ];

    window.bedroomHalfDonut.update();
    window.sunroomHalfDonut.update();
    window.studyHalfDonut.update();
    window.studioHalfDonut.update();
    window.atticHalfDonut.update();
    window.totalHalfDonut.update();
    window.solarHalfDonut.update();


    TIME_INDEX++;

  }

  setInterval(nextMinute, 2000);

});
